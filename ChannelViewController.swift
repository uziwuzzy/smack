//
//  ChannelViewController.swift
//  Smack
//
//  Created by Muhammad Fauzi Masykur on 9/28/17.
//  Copyright © 2017 Muhammad Fauzi Masykur. All rights reserved.
//

import UIKit

class ChannelViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
    }

}
